package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);

            if (scanner.nextLine().length() >= 10) {
                throw new RuntimeException("Длина строки превышает 10 символов!");
            }
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }
}